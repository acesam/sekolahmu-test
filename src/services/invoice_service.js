import apiHelperNew from "./fetch_api";

export default {
  getInvoices(qs = null) {
    return apiHelperNew.get("/invoice", qs);
  }
};
