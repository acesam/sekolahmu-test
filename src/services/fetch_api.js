import queryString from "query-string";

const API_URL = "https://api.dev.sekolah.mu/se-test/";

function apiJaki(endpoint, config) {
  return fetch(API_URL + endpoint, config)
    .then(async (response) => {
      if (!response.ok) {
        switch (response.status) {
          case 400:
            const res = response.json();
            return { error: res };
            break;
          case 401:
            window.location.href = "/logout";
            throw new Error("Session Expired");
            break;
          case 403:
            throw new Error("Tidak ada permission");
            break;
          case 404:
            throw new Error("Request/Data tidak ditemukan");
            break;
          case 422:
            return response.json();
            break;
          case 500:
            throw new Error("Terjadi kesalahan pada sistem");
            break;
          case 502:
            throw new Error("Server tidak ditemukan");
            break;
          default:
            throw new Error(response.status);
            break;
        }
      } else {
        const res = response.headers.get("content-type");
        return response.json();
      }
    })
    .catch((error) => {
      return {
        error
      };
    });
}

export default {
  get(endpoint, qs = null) {
    let params = "";
    if (qs) {
      params = queryString.stringify(qs);
    }
    let config = {
      method: "GET",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("authToken")}`
        // 'Content-Type': 'application/json'
      }
    };
    return apiJaki(`${endpoint}?${params}`, config);
  },

  delete(endpoint) {
    let params = "";
    let config = {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("authToken")}`
        // 'Content-Type': 'application/json'
      }
    };
    return apiJaki(`${endpoint}?${params}`, config);
  },

  post(endpoint, data = null) {
    let params = "";

    var formData = new FormData();

    if (Array.isArray(data) && data.length > 0) {
      data.map((item) => {
        for (var key in item) {
          formData.append(key, item[key]);
        }
      });
    } else {
      for (var key in data) {
        formData.append(key, data[key]);
      }
      for (let key in data) {
        if (data[key] instanceof Array) {
          data[key].map((item) => {
            formData.append(key, item);
          });
        }
      }
    }
    let config = {
      method: "POST",
      headers: { Authorization: "secret_auth_token!!$$" },
      body: formData
    };
    return apiJaki(`${endpoint}?${params}`, config);
  },

  put(endpoint, data = null) {
    let params = "";

    var formData = new FormData();

    if (Array.isArray(data) && data.length > 0) {
      data.map((item) => {
        for (var key in item) {
          formData.append(key, item[key]);
        }
      });
    } else {
      for (var key in data) {
        formData.append(key, data[key]);
      }
      for (let key in data) {
        if (data[key] instanceof Array) {
          data[key].map((item) => {
            formData.append(key, item);
          });
        }
      }
    }
    let config = {
      method: "PUT",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("authToken")}`
        // 'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    };
    return apiJaki(`${endpoint}?${params}`, config);
  }
};
