import React, { useEffect, useState } from "react";
import { InvoiceServices } from "./services/index";
import {
  Row,
  Col,
  Card,
  CardBody,
  CardTitle,
  CardText,
  Table
} from "reactstrap";

const App = (props) => {
  const [dataInvoice, setDataInvoice] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    try {
      let invoices = "";
      const response = await InvoiceServices.getInvoices({});
      invoices = response?.data;
      setDataInvoice(invoices);
    } catch (error) {
      // props.logoutUser(props.history)
      console.log(error);
    }
  };

  console.log(dataInvoice, "bla");
  return (
    <React.Fragment>
      <Row>
        <img src="../../img/logo.png" alt="Logo" />
      </Row>
      <Row>
        <h4> Detail Pembelian</h4>
        <hr />
      </Row>
      <Row>
        <p>
          No.Tagihan :{" "}
          <span>
            <b>Cleo</b>
          </span>
        </p>
        <p>
          Nama Pengguna :{" "}
          <span>
            <b>Cleo</b>
          </span>
        </p>
        <p>
          Tanggal Pembelian :{" "}
          <span>
            <b>Cleo</b>
          </span>
        </p>
      </Row>

      <Row>
        <h4> Pembayaran Berhasil</h4>
        <hr />
      </Row>
      <Row>
        <Card>
          <CardBody>
            <CardTitle tag="h5"> Metode Pembayaran</CardTitle>
            <CardText>Bank Transfer</CardText>
          </CardBody>
        </Card>
      </Row>

      <Row>
        <h4> Detail Program</h4>
        <hr />
        <p>
          Penyedia Program <span>Sekolah.mu</span>
        </p>
      </Row>
      <Row>
        <Card>
          <CardBody>
            <Table borderless>
              <tbody>
                <tr>
                  <th scope="row">Fun With Number </th>
                  <td>RP 50.000</td>
                </tr>
                <tr>
                  <th scope="row">Fun With Number</th>
                  <td>RP 50.000</td>
                </tr>
                <tr>
                  <th scope="row">Fun With Number</th>
                  <td>RP 50.000</td>
                </tr>
                <hr />
                <tr>
                  <th scope="row">Subtotal</th>
                  <td>RP 50.000</td>
                </tr>
              </tbody>
            </Table>
          </CardBody>
        </Card>
      </Row>

      <hr />

      <Row>
        <Table borderless>
          <tbody>
            <tr>
              <th scope="row">Fun With Number</th>
              <td>RP 50.000</td>
            </tr>
            <tr>
              <th scope="row">Fun With Number</th>
              <td>RP 50.000</td>
            </tr>
            <tr>
              <th scope="row">Fun With Number</th>
              <td>RP 50.000</td>
            </tr>
            <hr />
            <tr>
              <th scope="row">Subtotal</th>
              <td>RP 50.000</td>
            </tr>
          </tbody>
        </Table>
      </Row>

      <Row>
        <h4>Kontak Kami</h4>
        <h4>08780809322(WA)</h4>
        <h4>suaramu@sekolahmu.co.id</h4>
      </Row>
    </React.Fragment>
  );
};

export default App;
